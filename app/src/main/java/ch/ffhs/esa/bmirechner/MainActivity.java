package ch.ffhs.esa.bmirechner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }

    public void calculateBMI(View button) {
        try {

            final TextView resultView = (TextView) findViewById(R.id.result);
            resultView.setText("");

            final EditText heightInput = (EditText) findViewById(R.id.heightInput);
            float height = Float.parseFloat(heightInput.getText().toString());
            height = height / 100;

            final EditText weightInput = (EditText) findViewById(R.id.weightInput);
            final float weight = Float.parseFloat(weightInput.getText().toString());


            // Calculate BMI
            float bmi = (weight / (height * height));

            resultView.append("Your BMI is: " + bmi);

        } catch(Exception e) {
            System.out.println(e);

        }


    }



}
